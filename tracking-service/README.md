# Baemin ReactJS

### Run

Universal: yarn start
SSG: yarn export

### Prepare
* installs extensions: eslint, es7+ React/Redux..., Sass/Less/Stylus/Pug..., Stylelint, TODO highlight.
* installs: npm install husky --save-dev ------(not)npx husky-init && yarn

### Libraries & Framework
* nextJS v12: https://nextjs.org/docs/getting-started
* react v18: https://reactjs.org/docs/getting-started.html
* axios: https://github.com/axios/axios
* emotion: https://emotion.sh/docs/introduction
* scss: https://sass-lang.com/guide
* zustand: https://github.com/pmndrs/zustand
* react testing: https://testing-library.com/docs 
* date-time: https://date-fns.org/

### Folder structure
* components: Atomic design (pages, templates, organisms, molecules, atoms )
* pages: https://nextjs.org/docs/basic-features/pages
* stores: includes: side effect, state management, context page...
* styles: Ui style GLOBAL, SCSS follow
* utils: includes: formatter, transform api, normalize data, constants. common function