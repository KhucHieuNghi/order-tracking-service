/* eslint-disable @typescript-eslint/no-var-requires */

const url = require('url');

const express = require('express');
const next = require('next');

const router = require('./routers');

const dev = process.env.NODE_ENV !== 'production';
const port = process.env.PORT || 3000 ;
const app = next({ dev });
const handle = app.getRequestHandler();
const server = express();

server.use(express.json());
server.use(express.urlencoded({ extended: false }));

app.prepare().then(() => {
  server.use(router);

  server.use((req, res) => {
    // Be sure to pass `true` as the second argument to `url.parse`.
    // This tells it to parse the query portion of the URL.
    const parsedUrl = url.parse(req.url, true);

    handle(req, res, parsedUrl);
  });

  server.listen(port, () => {
    console.log(`> Ready on http://localhost:${ port }`);
  });
});
