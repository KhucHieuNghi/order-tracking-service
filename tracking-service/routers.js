/* eslint-disable new-cap */
const express = require('express');

const handler = require('./controllers');

const router = express.Router();

router.route('/api/stream')
  .get(handler.subscriber);

router.route('/api/stream/:subscriberId')
  .put(handler.subscriberQuery);

router.route('/api/stream-dashboard')
  .get(handler.subscriberDashboard);

router.route('/api/dashboard')
  .get(handler.getDashboard);

router.route('/api/orders')
  .get(handler.getOrders)
  .post(handler.createOrder);

router.route('/api/orders/:orderId')
  .get(handler.getOrder)
  .put(handler.nextStatus)
  .delete(handler.cancelOrder);

module.exports = router;