/* eslint-disable no-const-assign */
/* eslint-disable @typescript-eslint/no-unused-vars */
const { format, differenceInSeconds, parse, differenceInMinutes } = require('date-fns');

let subscribers = [];

let subscribersDashboard = [];

let totalUserConnected = 0;

const ORDER_STATUS = {
  CREATED: 'CREATED',
  ACCEPTED: 'ACCEPTED',
  DRIVER_ASSIGNED: 'DRIVER_ASSIGNED',
  DELIVERING: 'DELIVERING',
  DONE: 'DONE',
  CANCELED: 'CANCELED',
};
  
const ORDER_NEXT_STATUS = {
  [ ORDER_STATUS.CREATED ]: 0,
  [ ORDER_STATUS.ACCEPTED ]: 1,
  [ ORDER_STATUS.DRIVER_ASSIGNED ]: 2,
  [ ORDER_STATUS.DELIVERING ]: 3,
  [ ORDER_STATUS.DONE ]: 4,
  [ ORDER_STATUS.CANCELED ]: 5,
};

const STATUS_WARNING_AFTER = 10;
const STATUS_FINISH_AFTER = 15;

const STATUS_WARNING_AFTER_DELIVERING = 30;
const STATUS_FINISH_AFTER_DELIVERING = 40;
  
const ORDER_NEXT_STATUS_NUMBER = Object.entries(ORDER_NEXT_STATUS).reduce((result, [ key, value ]) => ({ ...result, [ value ]: key }), {});

const FORMAT_TIME = 'MM/dd/yyyy HH:mm:ss';

let orders = [
  {
    orderId: 'BAEMIN-123',
    status: ORDER_STATUS.CREATED,
    updatedTime: '07/04/2022 04:32:00',
    riderName: 'Khúc Hiếu Nghi',
    riderPhone: '01244242',

    orderName: 'NMQ',
    orderPhone: '01234455',
    orderAddress: '123 Phan Huy Ích F15, Q TB',

    merchantName: 'Bà bán hàng',
    merchantAddress: '1000 Phan Huy Ích F15, Q TB',
    merchantPhone: '01234455',

    dishes: [
      {
        name: 'Chả giò',
        size: 'Phần trung',
        price: 20000,
        amount: 1
      },
      {
        name: 'Chân gà',
        size: 'Phần lớn',
        price: 10000,
        amount: 3
      },
    ],

    price: 20000,
    paymentMethod: 'MOMO'
  }
];

//! /api/subscriber

function sentEvent(response, data) {
  response.write(`data: ${ JSON.stringify(data) }\n\n`);
}

function sentEventSubscriber(payload) {
  // eslint-disable-next-line no-shadow
  subscribers.forEach(subscriber => sentEvent(subscriber.response, payload)  );
}

function sortOrder(){
  orders = orders.sort((prevOrder, nextOrder) => {
    const prevOrderTime = parse(prevOrder.updatedTime, FORMAT_TIME, new Date());

    const nextOrderTime = parse(nextOrder.updatedTime, FORMAT_TIME, new Date());

    const diffTime = differenceInSeconds(prevOrderTime, nextOrderTime);
    if(diffTime > 1) {return -1;}
    if(diffTime < 1) {return 1;}
    return 0;
  });
}

function subscriber(request, response) {

  const headers = {
    'Content-Type': 'text/event-stream',
    'Connection': 'keep-alive',
    'Cache-Control': 'no-cache'
  };
  response.writeHead(200, headers);

  const subscriberId = `Customer-${ (new Date()).getTime() }`;
  
  sentEvent(response, { id: subscriberId, type: 'INITIAL' });
  totalUserConnected+=1;

  subscribers.push({
    id: subscriberId,
    response
  });

  request.on('close', () => {
    console.log(`${ subscriberId } Connection closed`);
    subscribers = subscribers.filter(sub => sub.id !== subscriberId);
  });
}

function subscriberQuery(request, response) {
  const subscriberId = request.params.subscriberId;

  const paramsQueries = request.body;

  const subscriberIndex = subscribers.findIndex(subscriber => subscriber.id === subscriberId);

  subscribers[ subscriberIndex ].query = paramsQueries;

  response.json({ success: true });
}

function calculateDashboard(){

  const totalTimes = orders.reduce((result, order) => {

    const date = new Date();

    const time = parse(order.updatedTime, FORMAT_TIME, date);

    const status = order.status;
  
    const leftTime = differenceInMinutes(date, time);
  
    if([ ORDER_STATUS.DONE, ORDER_STATUS.CANCELED ].includes(status)){
      return { ...result, normal: result.normal + 1 };
    }
  
    if([ ORDER_STATUS.CREATED, ORDER_STATUS.ACCEPTED, ORDER_STATUS.DRIVER_ASSIGNED ].includes(status) && leftTime >= STATUS_FINISH_AFTER){
      return { ...result, late: result.late + 1 };
    }
  
    if([ ORDER_STATUS.DELIVERING ].includes(status) && leftTime >= STATUS_FINISH_AFTER_DELIVERING){
      return { ...result, late: result.late + 1 };
    }
  
    if([ ORDER_STATUS.CREATED, ORDER_STATUS.ACCEPTED, ORDER_STATUS.DRIVER_ASSIGNED ].includes(status) && leftTime >= STATUS_WARNING_AFTER){
      return { ...result, warning: result.warning + 1 };
    }
  
    if([ ORDER_STATUS.DELIVERING ].includes(status) && leftTime >= STATUS_WARNING_AFTER_DELIVERING){
      return { ...result, warning: result.warning + 1 };
    }

    return { ...result, normal: result.normal + 1 };
  }, {
    normal: 0,
    warning: 0,
    late: 0
  });

  const totalStatuses = orders.reduce((result, order) => {

    const status = order.status;

    if(status === ORDER_STATUS.CREATED){
      return { ...result, created: result.created + 1 };
    }
    if(status === ORDER_STATUS.ACCEPTED){
      return { ...result, accepted: result.accepted + 1 };
    }
    if(status === ORDER_STATUS.DRIVER_ASSIGNED){
      return { ...result, driverAssigned: result.driverAssigned + 1 };
    }
    if(status === ORDER_STATUS.DELIVERING){
      return { ...result, delivering: result.delivering + 1 };
    }
    if(status === ORDER_STATUS.DONE){
      return { ...result, done: result.done + 1 };
    }
    if(status === ORDER_STATUS.CANCELED){
      return { ...result, cancel: result.cancel + 1 };
    }
  
  }, {
    created: 0,
    accepted: 0,
    driverAssigned: 0,
    delivering: 0,
    done: 0,
    cancel: 0
  });

  return {
    totalUsersConnecting: subscribers.length,
    totalUserConnected,
    totalStatuses,
    totalTimes,
  };
}

function subscriberDashboard(request, response) {

  const headers = {
    'Content-Type': 'text/event-stream',
    'Connection': 'keep-alive',
    'Cache-Control': 'no-cache'
  };
  response.writeHead(200, headers);

  const subscriberId = `Dashboard-${ (new Date()).getTime() }`;

  setInterval(() => {
    response.write(`data: ${ JSON.stringify(calculateDashboard()) }\n\n`);
  }, 10000);
  
  response.write(`data: ${ JSON.stringify(calculateDashboard()) }\n\n`);
  
  subscribersDashboard.push({
    id: subscriberId,
    response
  });

  request.on('close', () => {
    console.log(`${ subscriberId } Connection closed`);
    subscribersDashboard = subscribersDashboard.filter(sub => sub.id !== subscriberId);
  });
}

function getDashboard(request, response) {
  response.json({ success: true, data: calculateDashboard() });
}

//! /api/orders
function getOrders(request, response) {
  
  sortOrder();

  response.json({ success: true, orders });
}

function createOrder(request, response) {

  const orderId = `BEAMIN-${ (new Date()).getTime() }`;
  
  const order = { 
    ...request.body, 
    status: ORDER_STATUS.CREATED,
    orderId, 
    updatedTime: format(new Date(), FORMAT_TIME) 
  };
  
  orders.push(order);

  sentEventSubscriber({ type: 'CREATE', order });

  response.json({ success: true, orderId });
}

//! /api/order/:id

function getOrder(request, response) {

  const order = orders.find(ord => ord.orderId === request.params.orderId);

  response.json({ success: true, order });
}

function nextStatus(request, response) {

  const orderIndex = orders.findIndex(ord => ord.orderId === request.params.orderId);

  const status = orders[ orderIndex ].status;

  if([ ORDER_STATUS.DONE, ORDER_STATUS.CANCELED ].includes(status)) {return response.json({ success: true });}
  
  orders[ orderIndex ].status = ORDER_NEXT_STATUS_NUMBER[ ORDER_NEXT_STATUS[ status ] + 1 ];
  orders[ orderIndex ].updatedTime = format(new Date(), FORMAT_TIME);

  sentEventSubscriber({  type: 'UPDATE', order: orders[ orderIndex ] });

  response.json({ success: true });
}

function cancelOrder(request, response) {

  const orderIndex = orders.findIndex(ord => ord.orderId === request.params.orderId);

  const status = orders[ orderIndex ].status;

  if([ ORDER_STATUS.DONE, ORDER_STATUS.CANCELED ].includes(status)) {return response.json({ success: true });}
  
  orders[ orderIndex ].status = ORDER_STATUS.CANCELED;
  orders[ orderIndex ].updatedTime = format(new Date(), FORMAT_TIME);

  sentEventSubscriber({  type: 'UPDATE', order: orders[ orderIndex ] });

  response.json({ success: true });
}

module.exports = {
  subscriberDashboard,
  subscriber,
  subscriberQuery,
  createOrder,
  nextStatus,
  cancelOrder,
  getOrder,
  getOrders,
  getDashboard,
};