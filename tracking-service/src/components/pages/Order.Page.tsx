import React from 'react';

import { OrderSortGroup, useEffectFilter } from '@/stores/orders';
import { Order } from '@/utils/constant';

import { OrderTemplate } from '../templates/Order.Template';

export const OrderPage = () => {

  const [ order, setOrder ] = React.useState<Order | undefined>();

  const [ chooseSortByOption, setChooseSortByOption ] = React.useState<OrderSortGroup | undefined>();

  const state = useEffectFilter();
  
  return (
    <React.Fragment>
      <OrderTemplate 
        { ...state }
        order={ order }
        setOrder={ setOrder }
        chooseSortByOption={ chooseSortByOption }
        setChooseSortByOption={ setChooseSortByOption }
      />
    </React.Fragment>
  );
};