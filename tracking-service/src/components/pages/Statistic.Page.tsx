import React from 'react';

import styled from '@emotion/styled';
import { Card } from 'antd';

import Button from '@/components/atoms/Button';
import { getDashboard } from '@/services';
import { connectEventSourceDashboard } from '@/utils/common';

interface Statistic {
  totalStatuses: {
    accepted: number
    cancel: number
    created: number
    delivering: number
    done: number
    driverAssigned: number
  }
  totalTimes: {
    late: number
    normal: number
    warning: number
  }
  totalUserConnected: number
  totalUsersConnecting: number
}

const StatisticStyled = styled.section`
  max-width: 550px;
  margin: auto;
  .ant-card{
    margin-bottom: 1rem;
  }
  .statistic-row{
    display: flex;
    justify-content: space-between;
  }
`;

const StatisticPage = () => {

  const [ state, setState ] = React.useState<Statistic>({
    totalStatuses: {
      accepted: 0,
      cancel: 0,
      created: 0,
      delivering: 0,
      done: 0,
      driverAssigned: 0
    },
    totalTimes: {
      late: 0,
      normal: 0,
      warning: 0,
    },
    totalUserConnected: 0,
    totalUsersConnecting: 0,
  });
  connectEventSourceDashboard((strStatistic: string) => {
    setState(JSON.parse(strStatistic));
  });

  const refresh = async () => {
    const statistic = await getDashboard();
    setState(statistic);
  };
  
  return (
    <StatisticStyled className=''>
      <div className='text-error'>{'*Note: Data refresh after 10s'}</div>

      <Card title={ <b>Statistics Users</b> } >
        <p className='statistic-row'>{'Users connected: '} <b>{state.totalUserConnected}</b></p>
        <p className='statistic-row'>{'Users connecting: '} <b>{state.totalUsersConnecting}</b></p>
      </Card>

      <Card title={ <b>Total Order by Status</b> } >
        <p className='statistic-row'>{'Total Created: '} <b>{state.totalStatuses.created}</b></p>
        <p className='statistic-row'>{'Total Accepted: '} <b>{state.totalStatuses.accepted}</b></p>
        <p className='statistic-row'>{'Total Driver Assigned: '} <b>{state.totalStatuses.driverAssigned}</b></p>
        <p className='statistic-row'>{'Total Delivering: '} <b>{state.totalStatuses.delivering}</b></p>
        <p className='statistic-row'>{'Total Done: '} <b>{state.totalStatuses.done}</b></p>
        <p className='statistic-row'>{'Total Canceled: '} <b>{state.totalStatuses.cancel}</b></p>
      </Card>

      <Card title={ <b>Total Order by Time</b> } >
        <p className='statistic-row'>{'Total Normal: '} <b>{state.totalTimes.normal}</b></p>
        <p className='statistic-row'>{'Total Warning: '} <b>{state.totalTimes.warning}</b></p>
        <p className='statistic-row'>{'Total Late: '} <b>{state.totalTimes.late}</b></p>
      </Card>
      <Button block type='primary' onClick={ refresh }>
        Refresh
      </Button>

    </StatisticStyled>
  );
};

export default StatisticPage;
