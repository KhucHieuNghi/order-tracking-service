import styled from '@emotion/styled';
import { Tag } from 'antd';

import { FilterState } from '@/stores/orders';

const OrderTagsFilterStyled = styled.section`
  padding-top: 0.5rem;
  padding-bottom: 0.5rem;
`;

interface OrderTagsFilterProps {
    tags: Pick<FilterState, 'filter'>['filter']
    delFilter: (field: string) => void
}

export const OrderTagsFilter = ({ tags, delFilter }: OrderTagsFilterProps) => (
  <OrderTagsFilterStyled className='order-tags-filter'>
    {
      tags?.map((filterOption, i) => (
        <Tag 
          closable 
          key={ i }
          onClose={ () => delFilter(filterOption.field) }
        >
          {`${ filterOption.field }: ${ filterOption.value }`}
        </Tag>
      ))
    }
  </OrderTagsFilterStyled>
);