import { Select } from 'antd';

import { OrderSortGroup } from '@/stores/orders';

import Button from '../atoms/Button';
import Input from '../atoms/Input';

interface OrderFilterProps {
    orderSortGroups: OrderSortGroup[]
    chooseSortByOption?: OrderSortGroup

    chooseSortGroup: (field: string) => void
    chooseSortBy: (value: string) => void
    addFilter: () => void
}

export const OrderFilter = (props: OrderFilterProps) => {
  const {
    orderSortGroups, 
    chooseSortByOption,

    chooseSortGroup,
    chooseSortBy,
    addFilter
  } = props;

  return (
    <section>
      <Select
        placeholder='Select field' 
        style={ { width: '20%' } } 
        onChange={ chooseSortGroup }>
        {
          orderSortGroups.map((orderSort) => <Select.Option 
            key={ orderSort.field } 
            value={ orderSort.field }
          >
            {orderSort.label}
          </Select.Option>)
        }
      </Select>
      {
        chooseSortByOption?.options ? 
          (
            <Select
              style={ { width: '50%', marginLeft:'1rem', marginRight:'1rem' } } 
              onChange={ chooseSortBy }
              placeholder='~'
            >
              {
                chooseSortByOption?.options.map((sortByOption: string) => 
                  <Select.Option 
                    key={ sortByOption } 
                    value={ sortByOption }
                  >
                    {sortByOption}
                  </Select.Option>)
              }
            </Select>
          ):
          (
            <Input
              value={ chooseSortByOption?.value }
              onChange={ (e) => chooseSortBy(e.target.value) }
              style={ {
                width: '50%',
                marginLeft:'1rem', marginRight:'1rem'
              } }
              placeholder="Search Text"
            />
          )
      }
      <Button type="primary" onClick={ addFilter }>Search</Button>
    </section>
  );
};