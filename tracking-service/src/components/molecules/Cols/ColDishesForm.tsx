import { Col, Row } from 'antd';

import { formatPrice } from '@/utils/common';

interface ColDishesFormProps {
    name: string
    label: string
    data: {
        amount: number
        name: string
        price: string | number
        size?: string
    }[]
}

export const ColDishesForm = ({ label, name, data }: ColDishesFormProps) => (
  <Row className={ name }>
    <Col span={ 24 }>
      <b>{label}</b>
    </Col>
    <Col span={ 24 }>
      {
        data.map((d, i) => (
          <Row key={ i }>
            <Col span={ 16 } className='dishes-info'>
              <div>{`${ d.amount }x`}</div>
              <div className='dishes-name'>
                <p>{d.name}</p>
                {Boolean(d.size) && <p className='dishes-size'>{d.size}</p>}
              </div>
            </Col>

            <Col span={ 8 } className='dishes-price price'>
              {formatPrice(d.price)}
            </Col>
          </Row>
        ))
      }
    </Col>
  </Row>
);