import { Col, Row } from 'antd';

interface ColInlineFormProps {
    className?: string
    name: string
    data: [
        {
            label: string
            value: string | number | JSX.Element
        },
        {
            label: string
            value: string | number | JSX.Element
        },
    ]
}

export const ColInlineForm = ({ data, name }: ColInlineFormProps) => (
  <Row className={ name }>
    {
      data.map((d, i) => (
        <Col span={ 12 } key={ i }>
          <Row>
            <Col span={ 10 }>
              {d.label}
            </Col>
            <Col span={ 12 }>
              {d.value}
            </Col>
          </Row>
        </Col>
      ))
    }
  </Row>
);