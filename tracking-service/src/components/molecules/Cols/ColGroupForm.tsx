import { Col, Row } from 'antd';

interface ColGroupFormProps {
  title: string
  name: string
  data: {
    label: string
    value: string | number | JSX.Element
  }[]
}

export const ColGroupForm = ({ title, data, name }: ColGroupFormProps) => (
  <Row className={ name }>

    <Col span={ 24 }>
      <b>{title}</b>
    </Col>
      
    {
      data.map((d, i) => (
        <Col span={ 24 } key={ i }>
          <Row>
            <Col span={ 6 }>
              {d.label}
            </Col>
            <Col span={ 18 }>
              {d.value}
            </Col>
          </Row>
        </Col>
      ))
    }
  </Row>
);