import styled from '@emotion/styled';
import { Form, Tag } from 'antd';
import { format } from 'date-fns';

import { formatPrice } from '@/utils/common';
import { DATE_FORMAT, Order, ORDER_STATUS_TAG, PAYMENT_METHOD_TAG } from '@/utils/constant';

import { ColDishesForm } from '../Cols/ColDishesForm';
import { ColForm } from '../Cols/ColForm';
import { ColGroupForm } from '../Cols/ColGroupForm';
import { ColInlineForm } from '../Cols/ColInlineForm';

const layout = {
  labelCol: { span: 6 },
  wrapperCol: { span: 24 },
};

const OrderDetailFormStyled = styled(Form)`

.dishes-info{
    display: flex;
    align-items: baseline;
}

.dishes-name{
    padding-left: 1rem;
}

.dishes-size{
  color: #7f8893;
}

.dishes-price{
    text-align: right;
}
`;

interface OrderDetailFormProps {
    order: Order
}

export const OrderDetailForm = ({ order }: OrderDetailFormProps) => {
  return (
    <OrderDetailFormStyled { ...layout } name="order-detail">
      <Form.Item>
        <ColForm
          name='orderId'
          label='Order Id:'
          value={ <b>{order.orderId}</b> }
        />
      </Form.Item>

      <Form.Item>
        <ColInlineForm
          name='status-updated-time'
          data={
            [
              {
                label: 'Status:',
                value: <Tag color={ ORDER_STATUS_TAG[ order?.status ].color }>
                  {ORDER_STATUS_TAG[ order?.status ].label}
                </Tag>
              },
              {
                label: 'Updated Time:',
                value: <b>{format( new Date(order.updatedTime), DATE_FORMAT.UPDATED_TIME)}</b>
              },
            ]
          }
        />
      </Form.Item>

      <Form.Item>
        <ColInlineForm
          name='price-payment-method'
          data={
            [
              {
                label: 'Price:',
                value: <p className="price">{formatPrice(order.price)}</p>
              },
              {
                label: 'Payment:',
                value:  <Tag color={ PAYMENT_METHOD_TAG[ order.paymentMethod ].color }>
                  {PAYMENT_METHOD_TAG[ order.paymentMethod ].label}
                </Tag>
              },
            ]
          }
        />
      </Form.Item>
      <Form.Item>
        <ColGroupForm
          title='Rider'
          name='rider'
          data={ [
            {
              label: 'Name:',
              value: order.riderName
            },
            {
              label: 'Phone:',
              value: order.riderPhone
            },
          ] }
        />
      </Form.Item>

      <Form.Item>

        <ColGroupForm
          title='Order'
          name='order'
          data={ [
            {
              label: 'Name:',
              value: order.orderName
            },
            {
              label: 'Phone:',
              value: order.orderPhone
            },
            {
              label: 'Address:',
              value: order.orderAddress
            },
          ] }
        />
      </Form.Item>

      <Form.Item>

        <ColGroupForm
          title='Merchant'
          name='merchant'
          data={ [
            {
              label: 'Name:',
              value: order.merchantName
            },
            {
              label: 'Phone:',
              value: order.merchantPhone
            },
            {
              label: 'Address:',
              value: order.merchantAddress
            },
          ] }
        />
      </Form.Item>

      <Form.Item>

        <ColGroupForm
          title='Rider'
          name='rider'
          data={ [
            {
              label: 'Name:',
              value: order.riderName
            },
            {
              label: 'Phone:',
              value: order.riderPhone
            },
          ] }
        />

      </Form.Item>

      <Form.Item>

        <ColDishesForm
          name='dishes'
          label='Dishes'
          data={ order.dishes }
        />
      </Form.Item>
    </OrderDetailFormStyled>
  );
};