/* eslint-disable no-return-assign */
/* eslint-disable no-param-reassign */
import React from 'react';

import styled from '@emotion/styled';
import { Form, Select, Tag } from 'antd';
import { useRouter } from 'next/router';

import Button from '@/components/atoms/Button';
import Input, { InputNumber } from '@/components/atoms/Input';
import { postOrder } from '@/services';
import { formatPrice } from '@/utils/common';
import { Dishes, DISHES, Order, ORDER_STATUS, ORDER_STATUS_TAG, PAYMENT_METHOD, PAYMENT_METHOD_TAG } from '@/utils/constant';

import { ColDishesForm } from '../Cols/ColDishesForm';
import { ColForm } from '../Cols/ColForm';
import { ColGroupForm } from '../Cols/ColGroupForm';
import { ColInlineForm } from '../Cols/ColInlineForm';

const layout = {
  labelCol: { span: 6 },
  wrapperCol: { span: 24 },
};

const OrderDetailFormStyled = styled(Form)`

.dishes-info{
    display: flex;
    align-items: baseline;
}

.dishes-name{
    padding-left: 1rem;
}

.dishes-size{
  color: #7f8893;
}

.dishes-price{
    text-align: right;
}
`;

interface OrderDetailFormProps {
    order?: Order
}

const dishesDefault = [
  { name: 'Cháo lòng', size: 'Nhỏ', price: 100000, amount: 1 },
  { name: 'Thịt mèo', price: 220000, amount: 1 }
];
export const OrderDetailCreateForm = (props: OrderDetailFormProps) => {

  const router = useRouter();

  const [ state, setState ] = React.useState<Partial<Order>>({
    ...props.order,
    orderId: '',
    status: ORDER_STATUS.CREATED,
    updatedTime: undefined,
    riderName: 'Tài xế thánh thiện',
    riderPhone: '111111111',

    orderName: 'Khách hàng may mắn',
    orderPhone: '222222222',
    orderAddress: 'Địa chỉ giao hàng',

    merchantName: 'Bà bán quán ăn xui xẻo',
    merchantAddress: 'Địa chỉ lấy đồ ăn',
    merchantPhone: '33333333333',

    dishes: dishesDefault,

    price: dishesDefault.reduce((sum, dish) => sum+=dish.price,0),
    paymentMethod: PAYMENT_METHOD.MOMO
  });

  const onChangeValue = (field: string, value: string | number | Dishes[] ) => {
    const newState = { ...state, [ field ]: value };

    const price = newState.dishes?.reduce((sum, dish) => sum+=dish.price,0);

    setState({ ...state, [ field ]: value, price });
  };

  const submit = async() => {
    const data = await postOrder(state as Order).catch(() => null);
    router.push(`/orders/${ data.orderId }`);
  };

  return (
    <OrderDetailFormStyled { ...layout } name="order-detail" className='order-detail-form'>
      <small className='text-error'>{'*Note: Data mockup so haven\'t validation yet. Please input full fields'}</small>
        
      <Form.Item>
        <ColForm
          name='orderId'
          label='Order Id:'
          value={ <b>{state.orderId || '~'}</b> }
        />
      </Form.Item>

      <Form.Item>
        <ColInlineForm
          name='status-updated-time'
          data={
            [
              {
                label: 'Status:',
                value: <Tag color={ ORDER_STATUS_TAG[ ORDER_STATUS.CREATED ].color }>
                  {ORDER_STATUS_TAG[ ORDER_STATUS.CREATED ].label}
                </Tag>
              },
              {
                label: 'Updated Time:',
                value: <b>~</b>
              },
            ]
          }
        />
      </Form.Item>

      <Form.Item>
        <ColInlineForm
          name='price-payment-method'
          data={
            [
              {
                label: 'Price:',
                value: <b>{formatPrice(state.price || 0)}</b>
              },
              {
                label: 'Payment:',
                value:  (
                  <Select defaultValue={ PAYMENT_METHOD.MOMO } style={ { width: '100%' } } onChange={ () => null }>
                    {
                      Object.entries(PAYMENT_METHOD_TAG).map(([ key, { label } ]) => (
                        <Select.Option key={ key } value={ key }>{label}</Select.Option>
                      ))
                    }
                    
                  </Select>
                )
              },
            ]
          }
        />
      </Form.Item>
      <Form.Item>
        <ColGroupForm
          title='Rider'
          name='rider'
          data={ [
            {
              label: 'Name:',
              value: <Input placeholder='Input Name' value={ state.riderName } onChange={ (e) => onChangeValue('riderName', e.target.value) } />
            },
            {
              label: 'Phone:',
              value: <InputNumber max={ 99999999999 } placeholder='Phone number' value={ state.riderPhone } onChange={ (value) => onChangeValue('riderPhone', value) } />
            },
          ] }
        />
      </Form.Item>

      <Form.Item>

        <ColGroupForm
          title='Order'
          name='order'
          data={ [
            {
              label: 'Name:',
              value: <Input placeholder='Input Name' value={ state.orderName } onChange={ (e) => onChangeValue('orderName', e.target.value) } />
            },
            {
              label: 'Phone:',
              value: <InputNumber max={ 99999999999 } placeholder='Phone number' value={ state.orderPhone } onChange={ (value) => onChangeValue('orderPhone', value) } />
            },
            {
              label: 'Address:',
              value: <Input placeholder='Input Address' value={ state.orderAddress } onChange={ (e) => onChangeValue('orderAddress', e.target.value) } />
            },
          ] }
        />
      </Form.Item>

      <Form.Item>

        <ColGroupForm
          title='Merchant'
          name='merchant'
          data={ [
            {
              label: 'Name:',
              value: <Input placeholder='Input Name' value={ state.merchantName } onChange={ (e) => onChangeValue('merchantName', e.target.value) } />
            },
            {
              label: 'Phone:',
              value: <InputNumber max={ 99999999999 } placeholder='Phone number' value={ state.merchantPhone } onChange={ (value) => onChangeValue('merchantPhone', value) } />
            },
            {
              label: 'Address:',
              value: <Input placeholder='Input Address' value={ state.merchantAddress } onChange={ (e) => onChangeValue('merchantAddress', e.target.value) } />
            },
          ] }
        />
      </Form.Item>

      <Form.Item>
        
        <Select
          style={ { width: '100%' } } 
          onChange={ (_, record: unknown) => {
            const { key } = record as unknown as {key: string};
            const dish =  DISHES.find(dis => dis.name === key);
            if(!dish) {return;}

            onChangeValue('dishes', [ ...(state.dishes || []), dish ]);
          } }
          placeholder='Select Dishes'
        >
          {
            DISHES.map((dis) => 
              <Select.Option 
                key={ dis.name } 
                value={ dis.name }
              >
                {`${ dis.name }, ${ dis.price }`}
              </Select.Option>
            )
          }
        </Select>
        
      </Form.Item>

      <Form.Item>

        <ColDishesForm
          name='dishes'
          label='Dishes'
          data={ state.dishes || [] }
        />
      </Form.Item>

      <Form.Item>
        <Button type='primary' block onClick={ submit }>
          Submit
        </Button>
      </Form.Item>
    </OrderDetailFormStyled>
  );
};