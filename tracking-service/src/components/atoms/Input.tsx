import styled from '@emotion/styled';
import { Input as InputAntd, InputNumber as InputNumberAntd, InputNumberProps, InputProps } from 'antd';

const InputStyled = styled(InputAntd)`
    border: 1px solid #3ac5c9 !important;
    border-radius: 8px !important;
`;

const InputNumberStyled = styled(InputNumberAntd)`
    width: 100%;
    border: 1px solid #3ac5c9 !important;
    border-radius: 8px !important;
`;
    
export const Input = (props: InputProps) => <InputStyled { ...props } />;

export const InputNumber = (props: InputNumberProps) => <InputNumberStyled controls={ false } { ...props } />;

export default Input;