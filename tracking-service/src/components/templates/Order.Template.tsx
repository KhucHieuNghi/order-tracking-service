import React from 'react';

import { Modal, notification, Space, Table, Tag } from 'antd';
import { ColumnsType, SortOrder } from 'antd/lib/table/interface';
import clsx from 'clsx';
import { format } from 'date-fns';
import Link from 'next/link';

import Button from '../atoms/Button';
import { OrderDetailForm } from '../molecules/Forms/OrderDetailForm';
import { OrderFilter } from '../organisms/OrderFilter';
import { OrderTagsFilter } from '../organisms/OrderTagsFilter';
import { cancelOrder } from '@/services';
import { FilterStore, OrderSortGroup, orderSortGroups, Pagination } from '@/stores/orders';
import { calculateUpdatedTime, formatPrice } from '@/utils/common';
import { DATE_FORMAT, Order, ORDER_STATUS, ORDER_STATUS_TAG, PAGE_SIZE, PAYMENT_METHOD_TAG, SORT_BY, UPDATE_TIME_STATUS } from '@/utils/constant';

interface OrderTemplateProps extends FilterStore {
    order: Order | undefined
    setOrder: (order: Order | undefined) => void

    chooseSortByOption: OrderSortGroup | undefined
    setChooseSortByOption: (option: OrderSortGroup | undefined) => void
}

export const OrderTemplate = (props: OrderTemplateProps) => {
  const {
    order, setOrder, chooseSortByOption, setChooseSortByOption,
    filter, data, pagination,
    addFilter,
    delFilter,
    changePagination,
    addSort,
  } = props;

  const openCancelForm = (orderId: string) => Modal.warning({
    closable: true,
    title: 'Cancel',
    content: `OrderId: ${ orderId }`,
    onOk: async () => {
      setOrder(undefined);
      await cancelOrder(orderId);
      notification.success({
        message: 'Cancel Success',
        description: '',
      });
      return null;
    }
  });

  const columns: ColumnsType<Order> = [
    {
      title: 'Order Id',
      dataIndex: 'orderId',
      key: 'orderId',
      sorter: true,
      showSorterTooltip: false
    },
    {
      title: 'Status',
      key: 'status',
      dataIndex: 'status',
      render: (value) => (
        <Tag color={ ORDER_STATUS_TAG[ value ].color }>
          {ORDER_STATUS_TAG[ value ].label}
        </Tag>
      )
    },
    {
      title: 'Updated Time',
      key: 'updatedTime',
      dataIndex: 'updatedTime',
      sorter: true,
      defaultSortOrder: SORT_BY.DESC as SortOrder,
      showSorterTooltip: {
        title: 'Baemin Updated Time Sort'
      },
      className: 'col-updated-time',
      onCell: (record:Order):any => {
        return {
          'data-orderid': record.orderId,
          'data-status': (ORDER_STATUS as Record<string, string>)[ record.status ]
        };
      },
      render: (value) =>{
        if(!value) {return '';}
        return format(new Date(value), DATE_FORMAT.UPDATED_TIME);
      }
    },
    {
      title: 'Rider Name',
      dataIndex: 'riderName',
      key: 'riderName',
      sorter: true,
      showSorterTooltip: false
    },
    {
      title: 'Order Name',
      dataIndex: 'orderName',
      key: 'orderName',
      sorter: true,
      showSorterTooltip: false
    },
    {
      title: 'Merchant Name',
      dataIndex: 'merchantName',
      key: 'merchantName',
      sorter: true,
      showSorterTooltip: false
    },
    {
      title: 'Payment',
      dataIndex: 'paymentMethod',
      key: 'paymentMethod',
      sorter: true,
      showSorterTooltip: false,
      render: (paymentMethod: string) => <Tag color={ PAYMENT_METHOD_TAG[ paymentMethod ].color }>
        {PAYMENT_METHOD_TAG[ paymentMethod ].label}
      </Tag>

    },
    {
      title: 'Price',
      key: 'price',
      dataIndex: 'price',
      sorter: true,
      showSorterTooltip: false,
      render: (price: Pick<Order, 'price'>['price']) => <Space><b>{formatPrice(price)}</b></Space>
    },
    {
      title: '',
      key: 'action',
      render: (record: Order) => (
        <Space direction='vertical' size='small'>
          <Button
            danger
            block
            size="small"
            onClick={ () => openCancelForm(record.orderId) }>
            Cancel
          </Button>
          <Button
            block
            type="primary"
            size="small"
            onClick={ () => setOrder(record) }
          >
            View
          </Button>
        </Space>
      ),
    },
  ];

  return (
    <React.Fragment>
      <OrderFilter
        orderSortGroups={ orderSortGroups }
        chooseSortByOption={ chooseSortByOption }

        chooseSortGroup={ (field: string) => setChooseSortByOption(orderSortGroups.find((orderSortGroup: OrderSortGroup) => orderSortGroup.field === field)) }
        chooseSortBy={ (value: string) => chooseSortByOption && setChooseSortByOption({ ...chooseSortByOption, value }) }
        addFilter={ () => chooseSortByOption && addFilter(chooseSortByOption) }
      />

      <OrderTagsFilter
        tags={ filter }
        delFilter={ delFilter }

      />

      <Table
        onChange={ (pagi, _, info, type) => {

          if (type.action === 'sort') {
            const sortInfo = info as OrderSortGroup & { order: string };
            return addSort({
              field: sortInfo?.field as string,
              value: sortInfo.order
            } as OrderSortGroup);
          }

          if (type.action === 'paginate') {
            return changePagination(pagi as Pagination);
          }

        } }

        onRow={ (record) => {

          const updatedTimeStatus = calculateUpdatedTime(record as unknown as Record<string, string>);

          return {
            key: record.orderId,
            className: clsx(`row-order-${ record.orderId }`, 
              {
                'row-warning': updatedTimeStatus === UPDATE_TIME_STATUS.WARNING,
                'row-error': updatedTimeStatus === UPDATE_TIME_STATUS.ERROR,
              }
            )
          };
        } }
        columns={ columns }
        dataSource={ data }
        pagination={ {
          defaultCurrent: 1,
          defaultPageSize: PAGE_SIZE,
          ...pagination,
        } }
      />

      <Modal
        closable={ false }
        title={ <div className='modal-order-detail'><span>Order Detail</span><Link href={ `orders/${ order?.orderId }` } passHref ><a target='_blank'><Tag color="#3ac5c9">Open order detail</Tag></a></Link></div> }
        visible={ Boolean(order) }
        onOk={ () => setOrder(undefined) }
        onCancel={ () => order?.orderId && openCancelForm(order?.orderId) }
        cancelButtonProps={ { danger: true } }
        okText="Okie"
        cancelText="Cancel"
      >
        {
          !!order && <OrderDetailForm order={ order } />
        }
      </Modal>
    </React.Fragment>
  );
};