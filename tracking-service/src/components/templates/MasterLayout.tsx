import { PropsWithChildren } from 'react';

import Link from 'next/link';

import styles from '@/styles/masterLayout.module.scss';

export const MasterLayout = (props: PropsWithChildren) => {
  return (
    <section className={ styles.masterLayout }>
      <header className={ styles.header }>
        <Link href={ '/' } passHref>
          <a>
            <figure className={ styles.logo }>
              <img src="https://baemin.vn/desktop/images/logo.svg" alt="" />
            </figure>
          </a>
        </Link>
        <section className={ styles.menus }>
          <Link href={ '/orders/create' } passHref>
            <a target="_blank">
              Create order
            </a>
          </Link>

          <Link href={ '/orders' }>
            Orders
          </Link>
        </section>
      </header>
      <section className={ styles.sContent }>

        <main className={ styles.sMain }>
          {props.children}
        </main>

      </section>

      <footer className={ styles.footer }>
        <figure className={ styles.logo }>
          <img src="https://baemin.vn/mobile/images/logo-woowa-brothers-vn.svg" alt="" />
        </figure>
        <section>
          <p>
            <b>Công ty TNHH Woowa Brothers Việt Nam</b>
            <br/>
            Trụ sở chính:Tầng 4 và Tầng 5, Friendship Tower, 31 Lê Duẩn, P. Bến Nghé, Quận 1, TP. Hồ Chí Minh, Việt Nam
          </p>
        </section>
      </footer>
    </section>
  );
};