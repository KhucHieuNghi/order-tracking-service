import { differenceInMinutes, parse } from 'date-fns';
import { useQuery } from 'react-query';
import create, { State } from 'zustand';
import { immer } from 'zustand/middleware/immer';

import { getOrders } from '@/services';
import { DATE_FORMAT, Order, ORDER_STATUS, PAGE_SIZE, PAYMENT_METHOD, SORT_BY } from '@/utils/constant';
export interface OrderSortGroup {
    field: string
    label: string
    options?: string[]
    value?: string
}

export const orderSortGroups:OrderSortGroup[] = [
  {
    field: 'orderId',
    label: 'Order Id',
  },
  {
    field: 'status',
    label: 'Status',
    options: Object.values(ORDER_STATUS)
  },
  {
    field: 'paymentMethod',
    label: 'Payment Method',
    options: Object.values(PAYMENT_METHOD)
  },
  {
    field: 'updatedTime',
    label: 'Last Update Time (min)',
    options: [ '5', '10', '15' ]
  },
];

export interface Pagination {
  current: number
  total: number
  pageSize: number
}

export interface FilterState {
    id: string 
    sort?: {
        field: string,
        value: string
    }
    filter: {
        field: string,
        value: string
    }[],
    pagination: Pagination,
    data: Order[]
}

export const initialFilterState: FilterState = {
  id: '',
  sort: {
    field: 'updatedTime',
    value: SORT_BY.DESC
  },
  filter: [],
  pagination: {
    current: 1,
    total: 2,
    pageSize: PAGE_SIZE
  },
  data: [],
};

export interface FilterStore extends State, FilterState {
  setId: (id: string) => void

  addFilter: (filter: OrderSortGroup) => void

  delFilter: (filterField: string) => void

  delSort: () => void
  addSort: (sortBy: OrderSortGroup) => void

  changePagination: (pagination: Pagination) => void

  updateData: (orders: Order[]) => void

  updateOrder: (order: Order) => void

  cancelOrder?: (orderId: string) => void

  createData: (order: Order) => void
  
}

export const isLastUpdatedTime = ({ updatedTime }: Order, lastTime: number) => {
  const date = new Date();

  const time = parse(updatedTime, DATE_FORMAT.UPDATED_TIME, date);

  const leftTime = differenceInMinutes(date, time);

  if(leftTime >= lastTime ){
    return true;
  }
  return false;
  
};

const calculateFilter = (orders: Order[], { filter, sort }: FilterStore) => {
  let ordersClone = JSON.parse(JSON.stringify(orders));
    
  if(filter.length){
    filter.map(({ field, value }) => {

      ordersClone = ordersClone.filter((order:Order) => {
        if(field === 'updatedTime'){
          return isLastUpdatedTime(order, +value);
        }
        return order[ field as keyof Order ] === value;
      });
    });
  }
  
  if(sort && sort?.field && sort?.value){
    ordersClone = ordersClone.sort((prevOrder:any, nextOrder:any) => {
      const prevOrderS = prevOrder[ sort.field ];
        
      const nextOrderS = nextOrder[ sort?.field ];
        
      if(prevOrderS < nextOrderS) { return sort?.value === 'ascend' ? -1 : 1; }
      if(prevOrderS > nextOrderS) { return sort?.value === 'ascend' ? 1 : -1; }
      return 0;
    
    });
  } 
  return JSON.parse(JSON.stringify(ordersClone));

};

const immerFn = () => immer<FilterStore>((set, get) => ({
  ...initialFilterState,
  setId: (id: string) => set(state => { 
    state.id = id;
  }),
  addFilter: (filter: OrderSortGroup) => set(state => { 
    const prevFitter = get().filter || [];

    const newFilter = prevFitter.filter(filterOption => filterOption.field !== filter.field);

    state.filter = [ ...newFilter, {
      field: filter.field,
      value: filter.value as string
    } ];

  }),
  delFilter: (filterField: string) => set(state => { 
    const prevFitter = get().filter || [];

    const newFilter = prevFitter.filter(filter => filter.field !== filterField);

    state.filter = newFilter;

  }),

  addSort: (sortBy: OrderSortGroup) => set(state => { 
    state.sort = {
      field: sortBy.field,
      value: sortBy.value as string
    };
  }),
  delSort: () => set(state => { 
    state.sort = undefined;
  }),

  changePagination: (pagination: Pagination) => set(state => { 
    state.pagination = pagination;
  }),

  updateData: (orders: Order[]) => set(state => { 
    const exPagination = (get().pagination || []);
    
    state.data = calculateFilter(orders, get());

    state.pagination = { ...exPagination, total: state.data.length };
  }),

  createData: (order: Order) => set(state => { 
    const orders = (get().data || []);

    const exPagination = (get().pagination || []);

    state.data = calculateFilter([ order, ...orders ], get());
    
    state.pagination = { ...exPagination, total: state.data.length };
  }),

  updateOrder: (order: Order) => set(state => { 
    
    const newOrders = (get().data || []).filter(d => d.orderId !== order.orderId);

    const exPagination = (get().pagination || []);

    state.data = calculateFilter([ order, ...newOrders ], get());

    state.pagination = { ...exPagination, total: state.data.length };
    
  }),

}));

export const useFilter = create(immerFn());

export const useEffectFilter = () => {

  const state = useFilter(props => props);

  const { filter, sort, pagination } = state;

  useQuery([ 'order', filter, sort, pagination ], async () => {

    const res = await getOrders({ subscriberId: state.id });

    return res.orders;
  }, {
    onSuccess: state.updateData
  });

  return state;
};
