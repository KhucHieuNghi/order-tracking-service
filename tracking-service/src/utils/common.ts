import React from 'react';

import { differenceInMinutes } from 'date-fns';
import parse from 'date-fns/parse';

import { useFilter } from '@/stores/orders';

import { DATE_FORMAT, Order, ORDER_STATUS,
  UPDATE_TIME_STATUS, URL_DASHBOARD_SSE, URL_SSE } from './constant';

export const formatNumber = (value: string | number) => {
  return String(value).replace(/(.)(?=(\d{3})+$)/g, '$1,');
};
    
export const formatPrice = (price: string | number) => `${ formatNumber(price) } đ`;

export const STATUS_WARNING_AFTER = 10;
export const STATUS_FINISH_AFTER = 15;

export const STATUS_WARNING_AFTER_DELIVERING = 30;
export const STATUS_FINISH_AFTER_DELIVERING = 40;

export const calculateUpdatedTime = ({ updatedTime, status }: Record<string, string>) => {
  const date = new Date();

  const time = parse(updatedTime, DATE_FORMAT.UPDATED_TIME, date);

  const leftTime = differenceInMinutes(date, time);

  if([ ORDER_STATUS.DONE, ORDER_STATUS.CANCELED ].includes(status)){
    return null;
  }

  if([ ORDER_STATUS.CREATED, ORDER_STATUS.ACCEPTED, ORDER_STATUS.DRIVER_ASSIGNED ].includes(status) && leftTime >= STATUS_FINISH_AFTER){
    return UPDATE_TIME_STATUS.ERROR;
  }

  if([ ORDER_STATUS.DELIVERING ].includes(status) && leftTime >= STATUS_FINISH_AFTER_DELIVERING){
    return UPDATE_TIME_STATUS.ERROR;
  }

  if([ ORDER_STATUS.CREATED, ORDER_STATUS.ACCEPTED, ORDER_STATUS.DRIVER_ASSIGNED ].includes(status) && leftTime >= STATUS_WARNING_AFTER){
    return UPDATE_TIME_STATUS.WARNING;
  }

  if([ ORDER_STATUS.DELIVERING ].includes(status) && leftTime >= STATUS_WARNING_AFTER_DELIVERING){
    return UPDATE_TIME_STATUS.WARNING;
  }

  return null;
  
};

interface Sse_Callback {
  type: string
  orderId: string
  order: Order
  id?: string
}

export const connectEventSource = (cb: (order: Sse_Callback) => void) => {
  React.useEffect(() => {
    if ('EventSource' in window) {
      const source = new EventSource(`${ window.location.origin }${ URL_SSE }`, { withCredentials: true });
      source.addEventListener('message', function(e) {     
        cb(JSON.parse(e.data));
      }, false);
      source.addEventListener('open', function() {
        console.log('open');
      }, false);
      source.addEventListener('error', function() {
        console.log('error');
      }, false);
      return () => {
        source.close();
      };
    }
  }, [ ]);
};

export const useWarningOrderInterval = () => {
  const { createData, updateOrder, setId } = useFilter(props => props);
  
  connectEventSource(({ type, order, id }: Sse_Callback) => {
    if(type === 'CREATE'){
      return createData(order);
    }

    if(type === 'UPDATE'){
      return updateOrder(order);
    }

    if(type === 'INITIAL'){
      return setId(id || '');
    }
  });
  
  React.useEffect(() => {

    const interval = setInterval(() => {

      const els = document.querySelectorAll(('.ant-table-tbody .col-updated-time')) as unknown as Element[];

      els.forEach((el)=> {

        const orderId = (el as unknown as { dataset: Record<string, string> })?.dataset?.orderid;
        const status = (el as unknown as { dataset: Record<string, string> })?.dataset?.status;

        const changeUpdatedStatus = calculateUpdatedTime({ updatedTime: el.innerHTML, status });

        const rowEl = document.querySelectorAll(`.row-order-${ orderId }`) as unknown as Element[];

        if(changeUpdatedStatus === UPDATE_TIME_STATUS.ERROR){
          rowEl[ 0 ]?.classList.remove('row-warning');
          rowEl[ 0 ]?.classList.add('row-error');
          return;
        }

        if(changeUpdatedStatus === UPDATE_TIME_STATUS.WARNING){
          rowEl[ 0 ]?.classList.add('row-warning');
          return;
        }

        rowEl[ 0 ]?.classList.remove('row-warning');
        return;

      });
    }, 5000);
    
    return () => {
      clearInterval(interval);
    };
  }, [ ]);
};

export const connectEventSourceDashboard = (cb: (statistics: string) => void) => {
  React.useEffect(() => {
    if ('EventSource' in window) {
      const source = new EventSource(`${ window.location.origin }${ URL_DASHBOARD_SSE }`, { withCredentials: true });
      source.addEventListener('message', function(e) {     
        cb(e.data);
      }, false);
      source.addEventListener('open', function() {
        console.log('open');
      }, false);
      source.addEventListener('error', function() {
        console.log('error');
      }, false);
      return () => {
        source.close();
      };
    }
  }, [ ]);
};