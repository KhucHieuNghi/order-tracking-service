export const PAGE_SIZE = 5;

export const ORDER_STATUS = {
  CREATED: 'CREATED',
  ACCEPTED: 'ACCEPTED',
  DRIVER_ASSIGNED: 'DRIVER_ASSIGNED',
  DELIVERING: 'DELIVERING',
  DONE: 'DONE',
  CANCELED: 'CANCELED',
};

export const ORDER_NEXT_STATUS = {
  [ ORDER_STATUS.CREATED ]: 0,
  [ ORDER_STATUS.ACCEPTED ]: 1,
  [ ORDER_STATUS.DRIVER_ASSIGNED ]: 2,
  [ ORDER_STATUS.DELIVERING ]: 3,
  [ ORDER_STATUS.DONE ]: 4,
  [ ORDER_STATUS.CANCELED ]: 5,
};

export const ORDER_NEXT_STATUS_NUMBER: Record<number, string> = Object.entries(ORDER_NEXT_STATUS).reduce((result, [ key, value ]) => ({ ...result, [ value ]: key }), {});
  
export const ORDER_STATUS_TAG = {
  [ ORDER_STATUS.CREATED ]: {
    color: 'cyan',
    label: 'Created'
  },
  [ ORDER_STATUS.ACCEPTED ]: {
    color: 'blue',
    label: 'Accepted'
  },
  [ ORDER_STATUS.DRIVER_ASSIGNED ]: {
    color: 'gold',
    label: 'Deriver assigned'
  },
  [ ORDER_STATUS.DELIVERING ]: {
    color: 'green',
    label: 'Delivering'
  },
  [ ORDER_STATUS.DONE ]: {
    color: 'geekblue',
    label: 'Done'
  },
  [ ORDER_STATUS.CANCELED ]: {
    color: 'black',
    label: 'Canceled'
  },
};

export const STATUS_WARNING = [ ORDER_STATUS.CREATED, ORDER_STATUS.ACCEPTED, ORDER_STATUS.DRIVER_ASSIGNED, ORDER_STATUS.DELIVERING ];

export const STATUS_WARNING_AFTER = 10;
export const STATUS_FINISH_AFTER = 15;

export const STATUS_WARNING_AFTER_DELIVERING = 30;
export const STATUS_FINISH_AFTER_DELIVERING = 40;

export const PAYMENT_METHOD = {
  MOMO: 'MOMO',
  ZALO: 'ZALO'
};

export const PAYMENT_METHOD_TAG = {
  [ PAYMENT_METHOD.MOMO ]: {
    color: 'magenta',
    label: 'Momo'
  },
  [ PAYMENT_METHOD.ZALO ]: {
    color: 'blue',
    label: 'Zalo Pay'
  },
};

export const DATE_FORMAT = {
  'UPDATED_TIME': 'MM/dd/yyyy HH:mm:ss',
};

export const UPDATE_TIME_STATUS = {
  NEXT: 'NEXT',
  WARNING: 'WARNING',
  ERROR: 'ERROR',
};

export const SORT_BY = {
  ASC: 'ascend' as string,
  DESC: 'descend' as string
};

export const URL_SSE = '/api/stream';

export const URL_DASHBOARD_SSE = '/api/stream-dashboard';

export const DISHES = [
  {
    name: 'Cháo lòng',
    size: 'Nhỏ',
    price: 100000,
    amount: 1
  },
  {
    name: 'Bò viên',
    price: 200000,
    amount: 1
  },
  {
    name: 'Thịt mèo',
    price: 220000,
    amount: 1
  }
];

export interface Dishes {
  name: string,
  size?: string
  price: number
  amount: number
}

export interface Order {
    orderId: string,
    status: string,
    updatedTime: string,
  
    riderName: string,
    riderPhone: string
  
    orderName: string,
    orderAddress: string,
    orderPhone: string
  
    merchantName: string,
    merchantAddress: string,
    merchantPhone: string
  
    dishes: Dishes[],
    
    price: number
  
    paymentMethod: string
  }
