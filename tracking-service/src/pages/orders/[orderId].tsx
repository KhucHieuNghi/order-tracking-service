import styled from '@emotion/styled';
import { Form, Space } from 'antd';
import { useRouter } from 'next/router';
import { useQuery } from 'react-query';

import Button from '@/components/atoms/Button';
import { OrderDetailForm } from '@/components/molecules/Forms/OrderDetailForm';
import { MasterLayout } from '@/components/templates/MasterLayout';
import { cancelOrder, getOrder, updateStatus } from '@/services';
import { ORDER_NEXT_STATUS, ORDER_NEXT_STATUS_NUMBER, ORDER_STATUS, ORDER_STATUS_TAG } from '@/utils/constant';

const CreateFormStyled = styled.section`
  display: flex;
  justify-content: space-between;
  align-items: center;
  .order-detail{
    max-width: 550px;
    margin: auto;
    text-align: left;
  }
  .ant-row{
    margin-top: 0.2rem;
    margin-bottom: 0.2rem;
  }
  .btns{
    width: 100%;
  }
`;

export const OrderDetail = () => {
  const router = useRouter();
  const { data, isSuccess, refetch } = useQuery([ 'order-detail', ], () => getOrder(router.query.orderId as string), {
    enabled: Boolean(router.query.orderId),
  });

  const submit = async () =>{
    await updateStatus(router.query.orderId as string);
    refetch();
  };

  const cancel = async () =>{
    await cancelOrder(router.query.orderId as string);
    refetch();
  };

  const buttonLabel = data?.order?.status && ORDER_STATUS_TAG[ ORDER_NEXT_STATUS_NUMBER[ ORDER_NEXT_STATUS[ data.order?.status ] + 1 ] ]?.label;

  return (
    <MasterLayout>
      <CreateFormStyled className="">
        <img
          srcSet="
                https://baemin.vn/desktop/images/baedale-cat-sherlock-01-pts@3x.png 3x,
                https://baemin.vn/desktop/images/baedale-cat-sherlock-01-pts@2x.png 2x,
                https://baemin.vn/desktop/images/baedale-cat-sherlock-01-pts.png    1x
              "
          src="https://baemin.vn/desktop/images/baedale-cat-sherlock-01-pts.png" 
          alt="BAEMIN Delicious Dishes"
        />
        {
          isSuccess &&
            (
              <Form className='order-detail'>
                <OrderDetailForm order={ data.order } />
                <Form.Item>
                  <Space direction='vertical' className='btns'>
                    {
                      Boolean(buttonLabel) && (
                        <Button type='primary' block onClick={ submit } disabled={ [ ORDER_STATUS.DONE, ORDER_STATUS.CANCELED ].includes(data?.order.status) }>
                          {buttonLabel}
                        </Button>
                      )
                    }

                    <Button danger block onClick={ cancel } disabled={ [ ORDER_STATUS.DONE, ORDER_STATUS.CANCELED ].includes(data?.order.status) }>
                      Cancel
                    </Button>
                  </Space>
                </Form.Item>
              </Form>
            )
            
        }
      
      </CreateFormStyled>
    </MasterLayout>
  );
};

export default OrderDetail;