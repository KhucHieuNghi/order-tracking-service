import React from 'react';

import styled from '@emotion/styled';

import { OrderDetailCreateForm } from '@/components/molecules/Forms/OrderDetailCreateForm';
import { MasterLayout } from '@/components/templates/MasterLayout';

const CreateFormStyled = styled.section`
  display: flex;
  justify-content: space-between;
  align-items: center;
  .order-detail-form{
    max-width: 550px;
    margin: auto;
    text-align: left;
  }
  .ant-row{
    margin-top: 0.2rem;
    margin-bottom: 0.2rem;
  }
`;

const CreateForm: React.FC = () => {
  return (
    <MasterLayout>
      <CreateFormStyled className="">
        <img
          srcSet="
                https://baemin.vn/desktop/images/baedale-cat-sherlock-01-pts@3x.png 3x,
                https://baemin.vn/desktop/images/baedale-cat-sherlock-01-pts@2x.png 2x,
                https://baemin.vn/desktop/images/baedale-cat-sherlock-01-pts.png    1x
              "
          src="https://baemin.vn/desktop/images/baedale-cat-sherlock-01-pts.png" 
          alt="BAEMIN Delicious Dishes"
        />
        <OrderDetailCreateForm />
      </CreateFormStyled>
    </MasterLayout>
  );
};

export default CreateForm;