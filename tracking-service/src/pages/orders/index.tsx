import type { NextPage } from 'next';

import { OrderPage } from '@/components/pages/Order.Page';
import { MasterLayout } from '@/components/templates/MasterLayout';
import { useWarningOrderInterval } from '@/utils/common';

const Home: NextPage = () => {
  useWarningOrderInterval();

  return (
    <MasterLayout>
      <OrderPage />
    </MasterLayout>
  );
};

export default Home;
