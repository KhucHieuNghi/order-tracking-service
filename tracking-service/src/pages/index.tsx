import type { NextPage } from 'next';

import StatisticPage from '@/components/pages/Statistic.Page';
import { MasterLayout } from '@/components/templates/MasterLayout';

const Home: NextPage = () => {
  return (
    <MasterLayout>
      <StatisticPage />
    </MasterLayout>
  );
};

export default Home;
