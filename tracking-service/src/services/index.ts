import axios from 'axios';

import { Order } from '@/utils/constant';

export const getOrders = (params?: Record<string, string>) => axios.get('/api/orders', {
  params
}).then(res => res.data);

export const postOrder = (order: Order) => axios.post('/api/orders',order).then(res=> res.data);

export const cancelOrder = (orderId: string) => axios.delete(`/api/orders/${ orderId }`);

export const updateStatus = (orderId: string) => axios.put(`/api/orders/${ orderId }`);

export const getOrder = (orderId: string) => axios.get(`/api/orders/${ orderId }`).then(res => res.data);

export const getDashboard = () => axios.get('/api/dashboard').then(res => res.data).then(res=> res.data);
