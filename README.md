# order-tracking-service

## Present: https://www.canva.com/design/DAFFffHvMRo/_2RzrlNLGqltdmT3g5DUEQ/view
## GIT: https://gitlab.com/KhucHieuNghi/order-tracking-service

## Getting started

Tech stacks: Ecosystem React
+ Framework: NextJS
+ API: NodeJS
+ Compiler: Webpack
+ Styled-component: Emotion
+ Side-effect: Axios, React-query
+ State-management: zustand
+ UI-Framework: Antd
+ Font: Baemin
+ ... Others

## Repositories

1. "Tracking script" Script embed to tracking order from other web
2. "Tracking service" Universal App, running with API and web application and dashboard

## How to run
cd tracking-service
yarn && yarn build && yarn start
or
npm i && npm run build && npm run start
or
docker build . -t tracking-service
docker run -p 3000:3000 tracking-service

open http://localhost:3000

and 

cd tracking-script
yarn && yarn build
or
npm i && npm run build

copy ./build/bundle.js.map to website <script src='./bundle' />

## Features
- Dashboard Realtime ( CCU, last updated time, order status) ----- "/"
- Order management screen Realtime ( Search, View, Cancel order, view order ) ------ "/orders"
- Form Create Order ( Create mockup order ) ---------------------------------- "/orders/create"
- Form Update and View Order ( Mockup Create order and change status ) ----------- "/orders/<BEADMIN-<id>>"

## Debt Issues
- Unittest
- Integration test
- Extra credit features
- Responsive medias


