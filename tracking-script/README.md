# Order tracking script

## Setup
- clone this repo
- install dependencies
```bash
yarn
```

## Dev
```bash
cd path/to/project
yarn dev
```

## Build
```bash
cd path/to/project

## Deploy
Remember to rename the bundle to `bundle.js`
