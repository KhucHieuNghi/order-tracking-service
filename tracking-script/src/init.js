import trackEventFromClick from './services/trackEventFromClick'

export default () => {
  // start tracking
  setTimeout(() => {
    window.addEventListener('click', trackEventFromClick)
  }, 100)
}
