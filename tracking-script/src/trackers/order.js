import trackEvent from '../services/trackEvent'
import { getReduxState } from '../utils/getReduxState'

export const createOrderTracker = async () => {
  try {
    const { order } = getReduxState()

    return trackEvent('CREATED', {
      path: '/api/order',
      method: 'POST'
    }, order)
  } catch (err) {
  }
}

export const cancelOrderTracker = async () => {
  try {
    const { order } = getReduxState()

    return trackEvent('CANCELED', {
      path: `/api/order/${order.id}`,
      method: 'DELETE'
    })
  } catch (err) {
    //
  }
}

export const updateOrderTracker = async () => {
  try {
    const { order } = getReduxState()

    return trackEvent('UPDATE', {
      path: `/api/order/${order.id}`,
      method: 'PUT'
    })
  } catch (err) {
    //
  }
}
