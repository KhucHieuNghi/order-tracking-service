const API_URL = process.env.BASE_URL

export default async (eventName, fetch, payload = {}) => {
  return fetch(`${API_URL}${fetch.path}`, {
    method: fetch.method,
    headers: {
      'Content-Type': 'application/json'
    },
    body: JSON.stringify(payload)
  })
}
