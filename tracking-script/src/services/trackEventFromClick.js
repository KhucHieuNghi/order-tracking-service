import { STATUS_TYPE } from '../constants'
import { cancelOrderTracker, createOrderTracker, updateOrderTracker } from '../trackers/order'

export default (e) => {
  const button = e.target.closest('button')
  if (!button) {
    return
  }
  const trackingEvent = button.dataset.trackingEvent
  if (!trackingEvent) {
    return
  }

  switch (trackingEvent) {
    case STATUS_TYPE.CREATED:
      createOrderTracker()
      break
    case STATUS_TYPE.CANCELED:
      cancelOrderTracker()
      break
    case STATUS_TYPE.ACCEPTED:
    case STATUS_TYPE.DRIVER_ASSIGNED:
    case STATUS_TYPE.DELIVERING:
    case STATUS_TYPE.DONE:
      updateOrderTracker()
      break
    default:
      // no event to track here
  }
}
