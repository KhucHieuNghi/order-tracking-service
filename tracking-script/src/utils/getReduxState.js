export const getReduxState = () => {
  try {
    return window.__NEXT_REDUX_STORE__.getState()
  } catch (err) {
    return {}
  }
}
